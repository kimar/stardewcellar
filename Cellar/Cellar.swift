//
//  Cellar.swift
//  Cellar
//
//  Created by Maxim Kershengolts on 10.11.17.
//  Copyright © 2017 Maxim Kershengolts. All rights reserved.
//

import Foundation

extension Array where Element == Int {
	var hash: Int {
		return reduce(5381) {
			($0 << 5) &+ $0 &+ Int($1)
		}
	}
}

typealias State = [Int]
private typealias Delta = (x: Int, y: Int)

private let kMaxAllowedPassage = 64
private let straightDeltas: [Delta] = [(x: -1, y: 0), (x: 0, y: -1), (x: 1, y: 0), (x: 0, y: 1)]
private let diagonalDeltas: [Delta] = [(x: -1, y: -1), (x: 1, y: -1), (x: 1, y: 1), (x: -1, y: 1)]

#if MOCKMAP
private let kMapWidth = 7
private let kMapHeight = 6
private let kInitialState: State = [ // 0 == Cask, 1 == Wall, 2 == Passage
	1, 1, 0, 2, 1, 1, 1,
	1, 0, 0, 0, 0, 1, 1,
	0, 0, 0, 0, 0, 0, 1,
	0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 0, 0, 0, 0
]
#else
private let kMapWidth = 18
private let kMapHeight = 12
private let kInitialState: State = [ // 0 == Cask, 1 == Wall, 2 == Passage
	1, 1, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
]
#endif

let kInitialRoom = kInitialState.reduce(0) {
	return $0 + (($1 == 1) ? 0 : 1)
}

class Map {
	let state: State
	let hash: Int
	private (set) var furtherStates: [State] = []
	private (set) var passage: Int = 0
	private (set) var allCasksAccessible: Bool = true
	
	var description: String {
		var s = "Casks: \(kInitialRoom - passage)\n"
		for y in 0..<kMapHeight {
			for x in 0..<kMapWidth {
				s += "\(state[y * kMapWidth + x]), "
			}
			s += "\n"
		}
		return s
	}

	static func initial() -> Map {
		return Map(state: kInitialState)
	}
	
	init(state: State) {
		self.state = state
		self.hash = state.hash
		setup()
	}
	
	private static func forNeighbours(_ x: Int, _ y: Int, diagonal: Bool, _ body: (_ nx: Int, _ ny: Int) -> Bool) {
		let deltas = straightDeltas + (diagonal ? diagonalDeltas : [])
		for delta in deltas {
			let ny = y + delta.y
			let nx = x + delta.x
			if (ny >= 0) && (ny < kMapHeight) && (nx >= 0) && (nx < kMapWidth) {
				if body(nx, ny) {
					return
				}
			}
		}
	}
	
	private func setup() {
		var furtherHashes: [Int] = []
		for y in 0..<kMapHeight {
			for x in 0..<kMapWidth {
				switch state[y * kMapWidth + x] {
				case 0: // Cask: checking if it is accessible
					var accessible: Bool = false
					Map.forNeighbours(x, y, diagonal: true) { nx, ny in
						if(state[ny * kMapWidth + nx] == 2) {
							accessible = true
							return true
						}
						return false
					}
					if !accessible {
						allCasksAccessible = false
					}
				case 2: // Passage: creating further maps by extending it in all directions and increasing the passage counter
					Map.forNeighbours(x, y, diagonal: false) { nx, ny in
						if(state[ny * kMapWidth + nx] == 0) {
							var newState = state
							newState[ny * kMapWidth + nx] = 2
							if !furtherHashes.contains(newState.hash) {
								furtherStates.append(newState)
								furtherHashes.append(newState.hash)
							}
						}
						return false
					}
					passage += 1
				default: // Wall: Doing nothing
					break
				}
			}
		}
	}

}

class Cellar {
	private var shortestPassage = Int.max
	private var processedHashes: [Int] = []
	var bestMaps: [Map] = []

	func findBestMaps(map: Map) {
		guard !processedHashes.contains(map.hash) else {
			return
		}
		processedHashes.append(map.hash)
		
		guard map.passage <= kMaxAllowedPassage else {
			return
		}
		
		if map.passage < shortestPassage {
			if map.allCasksAccessible {
				shortestPassage = map.passage
				bestMaps = [map]
			} else {
				for state in map.furtherStates {
					let furtherMap = Map(state: state)
					guard furtherMap.passage < shortestPassage else {
						break
					}
					findBestMaps(map: furtherMap)
				}
			}
		} else if (map.passage == shortestPassage) && map.allCasksAccessible {
			bestMaps.append(map)
		}
	}
	
}
