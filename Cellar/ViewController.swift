//
//  ViewController.swift
//  Cellar
//
//  Created by Maxim Kershengolts on 22/11/16.
//  Copyright © 2016 Maxim Kershengolts. All rights reserved.
//

import Cocoa

extension TimeInterval {
	var string: String {
		get {
			let seconds = Int(self.truncatingRemainder(dividingBy: 60))
			let minutes = Int((self / 60).truncatingRemainder(dividingBy: 60))
			let hours = Int(self / 3600)
			return String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, seconds)
		}
	}
}

class ViewController: NSViewController {
	@IBOutlet var textView: NSTextView!
	@IBOutlet var button: NSButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()

		textView.string.append("Max casks: \(kInitialRoom)\n")
	}
	
	@IBAction func start(sender: NSObject) {
		button.isEnabled = false
		textView.string.append("Searching for best maps...\n")
		let date = Date()
		DispatchQueue.global().async { [unowned self] in
			let cellar = Cellar()
			cellar.findBestMaps(map: Map.initial())
			DispatchQueue.main.async {
				self.textView.string.append("Best maps found: \(cellar.bestMaps.count), time spent: \(Date().timeIntervalSince(date).string)\n")
				for map in cellar.bestMaps {
					self.textView.string.append(map.description)
				}
				self.button.isEnabled = true
			}
		}
	}
	
	
}

